package com.thomas.bell.targetdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TargetdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TargetdemoApplication.class, args);
	}

}
